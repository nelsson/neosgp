export const routesApi = {
  pages: `${process.env.REACT_APP_API_ACF_URL}pages/`,
  login: process.env.REACT_APP_JWT_LOGIN_URL,
  urlApi: process.env.REACT_APP_API_URL,
  urlApiAcf: process.env.REACT_APP_API_ACF_URL,
};
