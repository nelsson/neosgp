export const colors = {
  primary: "#222222",
  secondary: "#066E8E",
  mono: {
    white: "#fff",
    graylight: "#A7A7A7"
  },
  brownlight: "#D49F4D",
  bluedark: "#073156"
};

export const fonts = {
  primary: "BeVietnam-SemiBold",
  secondary: "Arial"
};

export const mediaQuery = {
  movil: "480px",
  tablet: "768px",
  desktop: "1024px"
};
