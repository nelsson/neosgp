const UrlFont = "/static/fonts/";

const Fonts = `
    @font-face {
        font-family: 'BeVietnam-SemiBold';
        src: url('${UrlFont}BeVietnam-SemiBold.eot');
        src: url('${UrlFont}BeVietnam-SemiBold.eot?#iefix') format('embedded-opentype'),
            url('${UrlFont}BeVietnam-SemiBold.svg#BeVietnam-SemiBold') format('svg'),
            url('${UrlFont}BeVietnam-SemiBold.ttf') format('truetype'),
            url('${UrlFont}BeVietnam-SemiBold.woff') format('woff'),
            url('${UrlFont}BeVietnam-SemiBold.woff2') format('woff2');
        font-weight: normal;
        font-style: normal;
    }
 

    @font-face {
        font-family: 'icomoon';
        src:  url('${UrlFont}icomoon.eot?vhryx6');
        src:  url('${UrlFont}icomoon.eot?vhryx6#iefix') format('embedded-opentype'),
            url('${UrlFont}icomoon.ttf?vhryx6') format('truetype'),
            url('${UrlFont}icomoon.woff?vhryx6') format('woff'),
            url('${UrlFont}icomoon.svg?vhryx6#icomoon') format('svg');
        font-weight: normal;
        font-style: normal;
    }
    [class^="icon-"], [class*=" icon-"] {
        font-family: 'icomoon' !important;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    .icon-car:before {
        content: "\\e905";
    }
    .icon-heart:before {
        content: "\\e906";
    }
    .icon-point:before {
        content: "\\e907";
    }
    .icon-search:before {
        content: "\\e908";
    }
    .icon-arrow-thin-right:before {
        content: "\\e901";
    }
    .icon-isotipo:before {
        content: "\\e904";
    }
    .icon-pinterest:before {
        content: "\\e903";
    }
    .icon-facebook:before {
        content: "\\e902";
    }
    .icon-instagram:before {
        content: "\\e900";
    }
    .icon-close:before {
        content: "\\e605";
    }
    .icon-social-twitter:before {
        content: "\\e604";
    }
    .icon-menu:before {
        content: "\\e602";
    }
`;
export default Fonts;
