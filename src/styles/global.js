import { createGlobalStyle } from "styled-components/macro";
import { colors, fonts } from "./theme";
import Fonts from "./fonts";

export const GlobalStyle = createGlobalStyle`
  ${Fonts}
  body{
    margin:0px;
    font-family: ${fonts.primary};
    font-weight: 400;
    color: ${colors.primary};
    font-size: 20px;
    -webkit-font-smoothing: antialiased;
    -webkit-tap-highlight-color: transparent;
  }
  a {
    text-decoration:none;
  }
  button:focus, select:focus, input:focus, textarea:focus{
    outline:none
  }  
  button {
    border: none;
    background: none;
    padding: 0;
  }

  figure {
    margin: 0;
    img {
      max-width: 100%;
      height: auto;
    }
  }

  ul {
    margin: 0px;
    padding: 0px;
  }
  li{
      list-style:none
  }

  .content{
      max-width: 1045px;
      width: 94%;
      margin: 0 auto;
  }
  .flex{
    display: flex;
    flex-flow:row wrap;
  }

`;
