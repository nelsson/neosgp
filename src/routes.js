import Home from './pages/Home';
import Nosotros from './pages/nosotros';
import Contacto from './pages/contacto';
import Login from './pages/login';

const publicRoutes = [
  {
    name: 'Login',
    component: Login,
    path: '/',
    exact: true,
    bodyClass: 'login-page',
    hideHeader: true,
    hideFooter: true,
  },
  {
    name: 'Home',
    component: Home,
    path: '/home',
    exact: true,
    bodyClass: 'home',
  },
  {
    name: 'Nosotros',
    component: Nosotros,
    path: '/nosotros',
    exact: true,
    bodyClass: 'nosotros-page',
  },
  {
    name: 'Contacto',
    component: Contacto,
    path: '/contacto',
    exact: true,
    bodyClass: 'contacto',
  },
];

export { publicRoutes };
