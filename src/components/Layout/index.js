import React from 'react';
import Header from '../Header';
import Footer from '../Footer';
import { GlobalStyle } from '../../styles/global';

function Layout({ Component, route, bodyClass, hideHeader, hideFooter }) {
  return (
    <div className={`layout ${bodyClass ? bodyClass : ''}`}>
      <GlobalStyle></GlobalStyle>
      {hideHeader ? null : <Header />}

      <Component route={route} />
      {hideFooter ? null : <Footer />}
    </div>
  );
}

export default Layout;
