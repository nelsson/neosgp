import React, { useState } from 'react';
import { Formik, Field, Form } from 'formik';
import { TextField } from 'formik-material-ui';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import Loading from '../Loading';
import { FormInner, CreateAccount, InputItem, ShowPassword } from './styles';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  root: {
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    width: '100%',
    padding: 30,
  },
});

const LoginForm = ({ className, user, fetchUser, defaultValues }) => {
  const [showPassword, setShowPassword] = useState(true);
  const styles = useStyles();
  return (
    <Container maxWidth='sm' className={styles.root}>
      <Paper elevation={3} className={styles.paper}>
        <Typography variant='h4' align='center' component='h4' gutterBottom>
          Ingresa a tu Cuenta
        </Typography>
        <Formik
          initialValues={defaultValues}
          validate={(values) => {
            const errors = {};

            if (!values.username) {
              errors.username = 'Campo Obligatorio';
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.username)
            ) {
              errors.username = 'Email invalido';
            }
            if (values.password.trim() === '') {
              errors.password = 'Campo Obligatorio';
            }

            if (Object.keys(errors).length > 0) return errors;
          }}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            console.log(values);
            setTimeout(() => {
              setSubmitting(false);
              fetchUser(values);
              // resetForm();
            }, 500);
          }}
        >
          {({ submitForm, isSubmitting, values, setFieldValue }) => (
            <Form className='formulary-share'>
              <FormInner>
                <InputItem>
                  <Field
                    name='username'
                    type='text'
                    placeholder='Correo Electrónico'
                    component={TextField}
                  />
                </InputItem>
                <InputItem>
                  <Field
                    name='password'
                    type='password'
                    placeholder='Contraseña'
                    component={TextField}
                  />
                  <ShowPassword
                    onClick={() => {
                      setShowPassword(!showPassword);
                    }}
                  >
                    <img
                      src={`/static/images/icon-eyes-${
                        showPassword ? 'on' : 'off'
                      }.svg`}
                      alt='Sr. Cambio'
                    />
                  </ShowPassword>
                </InputItem>
              </FormInner>
              <Typography
                variant='subtitle1'
                align='center'
                component='p'
                gutterBottom
              >
                <Link to=''>¿Olvidaste tu Contraseña?</Link>
              </Typography>
              <Button
                type='submit'
                disabled={isSubmitting}
                variant='contained'
                color='primary'
                onClick={() => submitForm}
              >
                Iniciar Sesión
                {isSubmitting && <Loading />}
              </Button>
            </Form>
          )}
        </Formik>
        <CreateAccount>
          <Link to=''>Registrate Aquí</Link>
        </CreateAccount>
      </Paper>
    </Container>
  );
};

LoginForm.propTypes = {
  className: PropTypes.string,
  customClass: PropTypes.string,
};
export default LoginForm;
