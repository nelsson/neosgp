import { connect } from 'react-redux';
import { fetchUser } from '../../store/users/operations';

import LoginForm from './LoginForm';

const defaultValues = {
  username: '',
  password: '',
};
const mapStateToProps = (state) => {
  return {
    user: state.userState.user,
    defaultValues: defaultValues,
  };
};
const mapDispatchToProps = (dispatch) => ({
  fetchUser: (data) => dispatch(fetchUser(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
