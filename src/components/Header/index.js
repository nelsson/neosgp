import React from "react";
import { Link } from "react-router-dom";
import { Title, NavList, MenuItem } from "./styles";

function Header() {
  return (
    <>
      <Title>Header</Title>
      <NavList>
        <MenuItem>
          <Link className="MenuLink" to="/">
            Inicio
          </Link>
        </MenuItem>
        <MenuItem>
          <Link className="MenuLink" to="/nosotros">
            Nosotros
          </Link>
        </MenuItem>
        <MenuItem>
          <Link className="MenuLink" to="/contacto">
            Contacto
          </Link>
        </MenuItem>
      </NavList>
    </>
  );
}

export default Header;
