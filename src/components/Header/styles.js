import styled from "styled-components/macro";


export const Title = styled.h3`
  color: blue;
`;
export const NavList = styled.ul`
  margin-top: 30px;
  display: flex;
  justify-content: center;
  border: 1px solid #000;
  margin-bottom: 30px;
`;
export const MenuItem = styled.li`
  margin: 0;
  margin-left: 10px;
  .MenuLink {
    color: red;
  }
`;
