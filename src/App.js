import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter,
} from 'react-router-dom';
import Layout from './components/Layout';
import { publicRoutes } from './routes';

function App() {
  return (
    <Router>
      <Switch>
        {publicRoutes.map((route, index) => (
          <Route
            key={index}
            exact={route.exact}
            path={route.path}
            render={() => (
              <Layout
                Component={route.component}
                hideHeader={route.hideHeader}
                hideFooter={route.hideFooter}
                route={route}
                bodyClass={route.bodyClass ? route.bodyClass : ''}
              />
            )}
          />
        ))}
      </Switch>
    </Router>
  );
}

export default withRouter(App);
