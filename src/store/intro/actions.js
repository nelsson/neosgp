import { REQUEST_SLIDER, RECEIVE_SLIDER } from './types';

export const requestBanners = () => ({
  type: REQUEST_SLIDER,
});

export const receiveBanners = (json) => ({
  type: RECEIVE_SLIDER,
  banners: json,
});
