import { requestBanners, receiveBanners } from './actions';
import { routesApi } from '../../routesApi';

export const fetchBanners = () => {
  return (dispatch) => {
    dispatch(requestBanners);
    return fetch(`${routesApi.pages}99`)
      .then((response) => response.json())
      .then((data) => {
        dispatch(receiveBanners(data.acf.slides_list));
      });
  };
};
