import { RECEIVE_OPERATION_SIMULATION } from './types';

export const receiveOperationSimulation = (data) => ({
  type: RECEIVE_OPERATION_SIMULATION,
  operationSimulation: data,
});
