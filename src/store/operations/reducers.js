import { RECEIVE_OPERATION_SIMULATION } from './types';

const INITIAL_STATE = {
  operationSimulation: null,
};

export const operationsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case RECEIVE_OPERATION_SIMULATION:
      return {
        ...state,
        operationSimulation: action.operationSimulation,
      };
    default:
      return state;
  }
};
