import { receiveOperationSimulation } from './actions';

export const sendSimulation = (data) => {
  return (dispatch) => {
    return dispatch(receiveOperationSimulation(data));
  };
};
