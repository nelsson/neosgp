import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
// import { devToolsEnhancer } from 'redux-devtools-extension';
import { userReducer } from './users/reducers';
import { sliderIntroReducer } from './intro/reducers';
import { operationsReducer } from './operations/reducers';
import logger from 'redux-logger';

const rootReducer = combineReducers({
  userState: userReducer,
  introState: sliderIntroReducer,
  operationState: operationsReducer,
});

const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware, logger)
);

export default store;
