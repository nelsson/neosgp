import { requestUser, receiveUser } from './actions';
import { routesApi } from '../../routesApi';

export const fetchUser = (data) => {
  return (dispatch) => {
    dispatch(requestUser());
    return fetch(routesApi.login, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        dispatch(receiveUser(json));
      });
  };
};
