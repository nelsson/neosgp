import {
  REQUEST_USER,
  RECEIVE_USER,
  REQUEST_CREATE_ACCOUNT,
  RECEIVE_CREATE_ACCOUNT,
} from './types';
const INITIAL_STATE = {
  user: null,
  createAccount: null,
  loading: false,
};

export const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQUEST_USER:
      return {
        ...state,
        loading:true,
      };
    case RECEIVE_USER:
      return {
        ...state,
        user: action.user,
        loading:false,
      };
    case REQUEST_CREATE_ACCOUNT:
      return {
        ...state,
        loading: true,
      };
    case RECEIVE_CREATE_ACCOUNT:
      return {
        ...state,
        createAccount: action.data,
        loading: false,
      };
    default:
      return state;
  }
};


